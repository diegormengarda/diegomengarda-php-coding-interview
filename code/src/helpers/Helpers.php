<?php

namespace Src\helpers;

use GuzzleHttp\Client;

class Helpers
{
    function putJson($users, $entity)
    {
        file_put_contents(dirname(__DIR__) . '/../scripts/' . $entity . '.json', json_encode($users, JSON_PRETTY_PRINT));
    }

    public static function arraySearchI($needle, $haystack, $column)
    {
        return array_search($needle, array_column($haystack, $column));
    }

    public function validateClient($client)
    {
        if (!$client['email'] || !filter_var($client['email'], FILTER_VALIDATE_EMAIL)) {
            return [
                'valid' => false,
                'error' => 'Invalid email'
            ];
        }
        $phone = $client['phone'] ?? null;
        $phoneValid = !!$phone;

        if ($phoneValid) {
            $guzzleClient = new Client([
                'base_uri' => "http://apilayer.net/api/"
            ]);
            $response = $guzzleClient->request('GET', "validate?access_key=fb34999b1d3e9e4376d0530f0c0e3555&number={$phone}&country_code=US");
            $result = json_decode($response->getBody()->getContents(), true);
            $phoneValid = $result['valid'] ?? false;
        }
        if (!$phoneValid) {
            return [
                'valid' => false,
                'error' => 'Invalid phone number'
            ];
        }
        return [
            'valid' => true
        ];
    }
}