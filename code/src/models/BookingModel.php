<?php

namespace Src\models;

use GuzzleHttp\Client;
use Src\helpers\Helpers;

class BookingModel
{

    private $bookingData;
    private $helper;

    function __construct()
    {
        $this->helper = new Helpers();
        $string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
        $this->bookingData = json_decode($string, true);
    }

    private function getBookingModel(): BookingModel
    {
        return new BookingModel();
    }

    private function getDogModel(): DogModel
    {
        return new DogModel();
    }

    public static function getBookings()
    {
        return (new BookingModel())->bookingData;
    }

    public function createBooking($data)
    {
        $bookings = $this->getBookings();
        $dogs = $this->getDogModel()->getDogs();
        $dogsAges = 0;
        $dogsFiltered = array_filter($dogs, function ($item) use ($data) {
            return $item['clientid'] === $data['clientid'];
        });
        array_map(function ($item) use (&$dogsAges) {
            $dogsAges += $item['age'];
        }, $dogsFiltered);
        $averageDogsAges = $dogsAges / count($dogsFiltered);
        if ($averageDogsAges < 10) {
            $data['price'] = $data['price'] * .9;
        }

        $data['id'] = end($bookings)['id'] + 1;
        $bookings[] = $data;

        $this->helper->putJson($bookings, 'bookings');

        return $data;
    }
}